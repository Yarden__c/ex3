<?php 
    class Htmlpage{
        protected $title;
        protected $body;
        function __construct($title="",$body=""){
            if($body!=""){$this->body=$body;}
            if($title!=""){$this->title=$title;}
            
            
        }
        public function view(){
            echo "<html>
            <head>
            <title>$this->title</title>
            </head>
            <body> <p>$this->body</p> </body>
            </html>";
        }
}
class coloredMessage extends Htmlpage{
    protected $color = 'black';
    public function __set($property,$value){
        if($property == 'color'){
            $colors = array('red','blue','pink','green'); 
            if(in_array($value, $colors)){
                $this->color = $value;
            }
            else{$this->body = "Invalid color. Please enter another color.";
         }
        }
    }
    public function view(){
        echo "<title>$this->title</title>";
        echo "<p style = 'color:$this->color'>$this->body</p>";
    }
}    
class sizeMessage extends coloredMessage{
    protected $size = '16';
    public function __set($property,$value){
        parent::__set($property,$value);
    if ($property == 'size') {
        if ($value > "10" && $value < "24") {
            $this->size=$value;
        }
        else {
            $this->body = "Invalid size. Please enter another size";
        }
    }
 }
    public function view(){
        echo "<title>$this->title</title>";
        echo "<p style ='font-size:$this->size;color:$this->color'>$this->body</p>";
}    

    }
?>

